import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';

export default function Login() {
	
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
		// console.log(email)
		// console.log(password1)
		// console.log(password2)

		// if all fields are populated and passwords match
		if(email !== '' && password !== '') {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	function loginUser(e){
		e.preventDefault()//prevent default form behavior so form does not submit

		setEmail("")
		setPassword("")
		
		alert('Login Successful')
	}

	return (
		<Form onSubmit={e => loginUser(e)}>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Enter Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{
				isActive ? 	
					<Button className="mt-3" variant="primary" type="submit" id="submitBtn">
					Login
					</Button>
					:
					<Button className="mt-3" variant="primary" id="submitBtn" disabled>
					Login
					</Button>	
			}
	
		</Form>
	)

}

