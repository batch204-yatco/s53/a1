import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';

// courseProp will capture courseProps in Courses.js
// Destructuring is done in the parameter to retrieve courseProp
export default function CourseCard({courseProp}){

	// console.log(props.courseProp)
	// console.log(courseProp)

	const {name, description, price} = courseProp;

	// Use state hook for this component to be able to store its state
	// States are used to keep track of info related to individual components
	// Syntax:
		// const [getter, setter] = useState(initialGetterValue)

	// when component mounts for first time, any associated states will undergo state change from null to given initial/default state

	// e.g. count below goes from null to 0 since 0 is initial state 

	const [count, setCount] = useState(0);

	const [seat, setSeat] = useState(30)

	// count is to view value in useState, setCount is to change it

	// Using state hook returns an array with the first element being a value and second element as a function that's used to change the value of first element
	console.log(useState(0));


	// Function that keeps track of enrollees for a course
	// By default JavaScript is synchronous as it executes code from the top of the file to the bottom and will wait for the completion of one expression before it proceeds to the next
	// Setter function for useStates are asynchronous allowing it to execute separately from other codes in the program. Changes value.
	// Getter is to view value
	// setCount function is being executed while console.log is already being completed resulting in the console to be behind by one count

	function enroll (){

		setCount(count + 1);
		// console.log ('Enrolles: '+ count)
		setSeat(seat - 1)
		// console.log ('Seats: '+ seat)
	}

// useEffect is similar to event listeners. Keeps track of state changes. Makes given code block happen when a state changes AND when a component first mounts (such as on initial page load)

// in the example below, since count and seats are in the array of useState, the code block will execute whenever those states change

// if array is blank, the code will be executed on component mount ONLY

// Do NOT omit the array completely

useEffect(() => {
	if (seat === 0){
		alert ('No more seats available')
	}
}, [count, seat])

// syntax:
/*
useEffect(() => {
code to be executed
}, [state(s) to monitor])
*/

	return(
		<Card className="mb-2">
		    <Card.Body>
		        <Card.Title>
		            {name}
		        </Card.Title>
		        <Card.Subtitle>
		            Description:
		        </Card.Subtitle>
		        <Card.Text>
		            {description}
		        </Card.Text>
		        <Card.Subtitle>
		            Price:
		        </Card.Subtitle>
		        <Card.Text>
		            PhP {price}
		        </Card.Text>
		        <Card.Text>
		            Enrollees: {count}
		        </Card.Text>
		        <Card.Text>
		            Seats: {seat}
		        </Card.Text>
		        <Button variant="primary" onClick={enroll}>Enroll</Button>
		    </Card.Body>
		</Card>
	)
}