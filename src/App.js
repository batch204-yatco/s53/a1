import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom' // "as" changes name so it's shorter
import './App.css';
import AppNavBar from './components/AppNavBar';
import Courses from './pages/Courses'
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Error from './pages/Error';
import Calc from './pages/Calculator'

/*
  All other components/pages will be contained in our main component: <App />
  
  <>..</> - Fragment which ensures that adjacent JSX elements will be rendered and avoid this error. This is if no Router
*/
function App() {
  return (
    <Router>
      <>
      <AppNavBar/>
      <Container>
        {/*Switch goes from top to bottom*/}
        <Switch>
          {/*Routes are envelopes with addresses (path) and contents (component) 
          Exact prevents partial matching*/}
          <Route exact path="/" component={Home}/>
          <Route exact path="/courses" component={Courses}/>
          <Route exact path="/register" component={Register}/>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/calculator" component={Calc}/>
          <Route component={Error}/>
        </Switch>
      </Container>
      </>
    </Router>
  );
}

export default App;
